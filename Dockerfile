ARG NGINX_IMAGE_TAG=1.19.4-alpine
FROM nginx:${NGINX_IMAGE_TAG}

ARG NGINX_CACHE_PATH="/var/nginx/cache"
ARG NGINX_ALIAS="/data"

ENV NGINX_LOCATION="/" \
	NGINX_ALIAS="${NGINX_ALIAS}" \
	NGINX_CACHE_PATH="${NGINX_CACHE_PATH}" \
	NGINX_CACHE_CLIENT_MAX_AGE="864000" \
	NGINX_PORT="80"

LABEL maintainer="ilorgar@gesplan.es"

RUN apk add --no-cache \
        bash=~5

COPY rootfs /
