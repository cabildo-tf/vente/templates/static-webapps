#!/bin/bash

echo "Creating directory for nginx cache: ${NGINX_CACHE_PATH}"
mkdir -p "${NGINX_CACHE_PATH}"

nginx_config_path='/etc/nginx/'

for fullfile in $(find ${nginx_config_path} -name '*.tmpl');
do 
	echo "Processing ${fullfile} file"
	path=$(dirname "${fullfile}")
	filename=$(basename -- "${fullfile}")
	extension="${filename##*.}"
	filename="${filename%.*}"
	envsubst < "${fullfile}" > "${path}/${filename}.conf"
	rm ${fullfile}
done