#!/bin/bash

if [[ -f "${NGINX_ALIAS}/config.min.json" ]]; then
	envsubst < "${NGINX_ALIAS}/config.min.json" > "${NGINX_ALIAS}/config.json"
fi
if [[ -f "${NGINX_ALIAS}/widgets/AddDataCategories/config.json" ]]; then
	mv "${NGINX_ALIAS}/widgets/AddDataCategories/config.json" "${NGINX_ALIAS}/widgets/AddDataCategories/config.min.json"
	envsubst < "${NGINX_ALIAS}/widgets/AddDataCategories/config.min.json" > "${NGINX_ALIAS}/widgets/AddDataCategories/config.json"
fi
